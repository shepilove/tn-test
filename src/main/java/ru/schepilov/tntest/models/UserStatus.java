package ru.schepilov.tntest.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "user_statuses")
public class UserStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    LocalDateTime changeStatusDateTime;
    String oldStatus;
    String newStatus;
    Integer userId;
}
