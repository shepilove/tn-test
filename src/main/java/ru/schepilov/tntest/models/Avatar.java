package ru.schepilov.tntest.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "avatars")
public class Avatar {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    String name;
}
