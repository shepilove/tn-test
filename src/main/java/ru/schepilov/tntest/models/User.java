package ru.schepilov.tntest.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    String firstName;
    String surName;
    String nickName;
    String avatar;
    String email;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_statuses_id", referencedColumnName = "id")
    private UserStatus userStatus;
}
