package ru.schepilov.tntest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.schepilov.tntest.models.User;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findByNickName(String nickName);

    List<User> getAllByUserStatus_NewStatus(String status);

    List<User> getAllByUserStatus_ChangeStatusDateTimeAfterAndUserStatus_NewStatus(LocalDateTime dateTime, String status);
}
