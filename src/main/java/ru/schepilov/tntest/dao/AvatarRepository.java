package ru.schepilov.tntest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.schepilov.tntest.models.Avatar;

@Repository
public interface AvatarRepository extends JpaRepository<Avatar, Integer> {
}
