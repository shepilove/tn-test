package ru.schepilov.tntest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.schepilov.tntest.models.User;
import ru.schepilov.tntest.service.impl.AvatarService;
import ru.schepilov.tntest.service.impl.UserService;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class Controller {

    private final AvatarService avatarService;
    private final UserService userService;

    @Value("${upload.path}")
    private String uploadPath;

    @PostMapping("/upload")
    public ResponseEntity addAvatar(@RequestParam("file") MultipartFile file) {
        return avatarService.save(file);
    }

    @PostMapping("/user")
    public ResponseEntity addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @PostMapping("/user/status/update")
    public ResponseEntity updateStatus(@RequestParam("userId") Integer userId, @RequestParam("status") String status){
        return userService.statusUpdate(userId, status);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity getUser(@PathVariable Integer id) {
return userService.getUser(id);
    }

    @PostMapping("/statistics")
    public ResponseEntity getStatistic(@RequestParam("status") String status, @RequestParam("date") String date){
        return userService.getStatisticsByStatus(status, date);
    }

}
