package ru.schepilov.tntest.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.schepilov.tntest.dao.UserRepository;
import ru.schepilov.tntest.dao.UserStatusRepository;
import ru.schepilov.tntest.models.User;
import ru.schepilov.tntest.models.UserStatus;
import ru.schepilov.tntest.service.IOuterApiService;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class OuterApiService implements IOuterApiService {
    private final UserRepository userRepository;
    private final UserStatusRepository userStatusRepository;

    public ResponseEntity statusUpdate(Integer userId, String status) {
        User user = userRepository.getOne(userId);
        UserStatus userStatus = userStatusRepository.findByUserId(userId);
        if (userStatus == null) {
            userStatus = new UserStatus();
        }
        userStatus.setUserId(userId);
        userStatus.setOldStatus(user.getUserStatus() == null ? null : user.getUserStatus().getNewStatus());
        userStatus.setNewStatus(status);
        userStatus.setChangeStatusDateTime(LocalDateTime.now());
        UserStatus save = userStatusRepository.save(userStatus);
        user.setUserStatus(save);
        userRepository.save(user);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ошибка обновления статуса!");
        }
        return ResponseEntity.status(HttpStatus.OK).body(userStatus);
    }
}
