package ru.schepilov.tntest.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.schepilov.tntest.dao.AvatarRepository;
import ru.schepilov.tntest.models.Avatar;
import ru.schepilov.tntest.models.ResponseMessage;
import ru.schepilov.tntest.service.IAvatarService;

import java.io.File;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class AvatarService implements IAvatarService {
    @Value("${upload.path}")
    private String uploadPath;

    private final AvatarRepository avatarRepository;

    public ResponseEntity save(MultipartFile file) {
        try {
            File uploadDirectory = new File(uploadPath);
            if (!uploadDirectory.exists()) {
                uploadDirectory.mkdir();
            }
            String fileUUID = UUID.randomUUID().toString();
            String resultFileName = fileUUID + "." + file.getOriginalFilename();
            File fileUpload = new File(uploadPath + "/" + resultFileName);
            file.transferTo(fileUpload);
            ResponseMessage responseMessage = new ResponseMessage();
            responseMessage.setMessage(fileUpload.getPath());
            Avatar avatar = new Avatar();
            avatar.setName(file.getName());
            avatarRepository.save(avatar);
            return ResponseEntity.status(HttpStatus.OK).body(responseMessage);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }


    }
}
