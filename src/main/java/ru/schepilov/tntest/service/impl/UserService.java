package ru.schepilov.tntest.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.schepilov.tntest.dao.UserRepository;
import ru.schepilov.tntest.models.User;
import ru.schepilov.tntest.service.IUserService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final UserRepository userRepository;
    private final OuterApiService outerApiService;

    public ResponseEntity getUser(Integer id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(user.get());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Пользователь с id = " + id + " не найден!");
    }

    public ResponseEntity addUser(User user) {
        List<User> byNickName = userRepository.findByNickName(user.getNickName());
        if (byNickName.size() > 0) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        } else {
            userRepository.save(user);
            outerApiService.statusUpdate(user.getId(), "Online");
            return ResponseEntity.status(HttpStatus.OK).body(user.getId());
        }
    }

    public ResponseEntity statusUpdate(Integer userId, String status) {
        return outerApiService.statusUpdate(userId, status);
    }

    public ResponseEntity getStatisticsByStatus(String status, String date) {
        if (status.equals("no") && date.equals("null")) {
            List<User> allUsers = userRepository.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(allUsers);
        }
        if (date.equals("null")) {
            List<User> userByStatus = userRepository.getAllByUserStatus_NewStatus(status);
            return ResponseEntity.status(HttpStatus.OK).body(userByStatus);
        } else {
            String dateModified = date.replace("T", " ");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime dateTime = LocalDateTime.parse(dateModified, formatter);
            List<User> userByStatusAndDate = userRepository.getAllByUserStatus_ChangeStatusDateTimeAfterAndUserStatus_NewStatus(dateTime, status);
            return ResponseEntity.status(HttpStatus.OK).body(userByStatusAndDate);
        }
    }

}
