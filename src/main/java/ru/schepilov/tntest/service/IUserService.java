package ru.schepilov.tntest.service;

import org.springframework.http.ResponseEntity;
import ru.schepilov.tntest.models.User;

public interface IUserService {
    ResponseEntity getUser(Integer id);

    ResponseEntity addUser(User user);

    ResponseEntity statusUpdate(Integer userId, String status);

    ResponseEntity getStatisticsByStatus(String status, String date);
}
