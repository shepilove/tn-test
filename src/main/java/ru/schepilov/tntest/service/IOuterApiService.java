package ru.schepilov.tntest.service;

import org.springframework.http.ResponseEntity;

public interface IOuterApiService {

    ResponseEntity statusUpdate(Integer userId, String status);
}
