package ru.schepilov.tntest.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface IAvatarService {

    ResponseEntity save(MultipartFile file);
}
